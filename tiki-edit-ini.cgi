#!/usr/bin/perl
use strict;
use warnings;
use File::Basename;

our (%access, %text, %in, %config);
our ($module_name);

require './virtualmin-tikimanager-lib.pl';
&ReadParse();

my $d;
if ($in{'dom'}) {
  $d = &virtual_server::get_domain($in{'dom'});
}
if (! $d) {
  &error("Domain not found.");
}

# TODO: abstract to use with tiki-save-ini.cgi
chdir($d->{'public_html_path'});
my $config = &tikimanager_tiki_get_config($d) || &error("Can't open local.php");
my $file = $config{'tiki_preferences_file'} || "../tikiconfig/prefs.ini";
if (-e $config->{'system_configuration_file'}) {
  $file = $config->{'system_configuration_file'};
}

my $data = "";
if (-e "$file") {
  $data = &ui_read_file_contents_limit({ 'file', $file });
}

# Page title, must be first UI thing
&ui_print_header(
  'for ' . '<a href="https://' . $d->{'dom'} .'" target="_blank">https://' . $d->{'dom'} .'</a>',
  "Editing " . basename($file), "", undef, 1, 1
);

print &ui_form_start("tiki-save-ini.cgi", "post", undef, ('data-encoding="utf-8"'));
print &ui_table_start(&html_escape("$file"), undef, 1);
print &ui_hidden("dom", $d->{'id'}), "\n";
print &ui_textarea("data", $data, 20, 80, undef, undef, "style='width: 100%' id='data'");
print &ui_hidden("cwd", dirname($file));
print &ui_table_row("", "", 2);
print &ui_table_row('Documentation:', '<a href="https://doc.tiki.org/System-Configuration"'
      . ' target="_blank">'
      . ' https://doc.tiki.org/System-Configuration'
      . '</a>'
);
print &ui_table_row("", "", 2);
print &ui_table_end();
print &ui_form_end([['save', 'Save'], ['save_close', 'Save and close']]);

&ui_print_footer(
  $d ? &virtual_server::domain_footer_link($d) : ( ),
  "index.cgi?dom=$in{'dom'}",
  $text{'index_the_information_page'}
);
