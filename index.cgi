#!/usr/bin/perl
use strict;
use warnings;

our (%access, %config, %text, %in);
our $module_name;

require './virtualmin-tikimanager-lib.pl';
&foreign_require("cron");
&ReadParse();

# check if user can access the page
# &can_domain($in{'dom'}) || &error($text{'contact_ecannot'});

my $d;
if ($in{'dom'}) {
  $d = &virtual_server::get_domain($in{'dom'});
}

# define page tabs
my @tabs = (
    ['management',       &text('index_management')],
    ['config',           &text('index_config')],
);

# Page title, must be first UI thing
&ui_print_header(
  'for ' . '<a href="https://' . $d->{'dom'} .'" target="_blank">https://' . $d->{'dom'} .'</a>',
  $text{'index_title'}, "", undef, 1, 1
);

#my @tiki_branches = [ map {[ $_, $_ ]} &tikimanager_get_installable_branches($d) ];
my @tiki_branches = &tikimanager_get_installable_branches($d);
my $latest_branch = $tiki_branches[@tiki_branches - 1];
my ($info) = &tikimanager_tiki_info($d);

my @instances = &tikimanager_tiki_list_cloneables($d);
my @cloneables;
foreach my $instance (@instances) {
  push(@cloneables, [
    $instance->{'instance_id'},
    "$instance->{'name'} ($instance->{'branch'})"
  ]);
}

if ($info && $info->{'instance_id'}) {
  #
  # When Tiki is installed by Tiki Manager, show Tiki info
  #
  print &ui_subheading("Information by Tiki Manager");
  print &ui_table_start();
  print &ui_table_row("", "", 2);
  print &ui_table_row($text{'tikimanager_instance_id'}, $info->{'instance_id'}, 2);
  print &ui_table_row($text{'tikimanager_webroot'},     $info->{'webroot'}, 2);
  print &ui_table_row($text{'tikimanager_tempdir'},     $info->{'tempdir'}, 2);
  print &ui_table_row($text{'tikimanager_phpexec'},     $info->{'phpexec'}, 2);
  print &ui_table_row($text{'tikimanager_type'},        $info->{'type'}, 2);
  print &ui_table_row($text{'tikimanager_branch'},      $info->{'branch'}, 2);
  print &ui_table_row($text{'tikimanager_date'},        $info->{'date'}, 2);
  print &ui_table_row($text{'tikimanager_revision'},    $info->{'revision'}, 2);
  print &ui_table_end();

  print &ui_subheading("Tiki Actions");
  print &ui_table_start();
  print &ui_table_row("", "", 2);

  print &ui_table_row(
    "Edit tiki.ini",
    &ui_form_start('./tiki-edit-ini.cgi', 'get', undef, ("data=unbuffered-header-processor"))
    . &ui_hidden('dom', $d->{'id'})
    . ui_submit("Edit", "edit", 0)
    . &ui_form_end(),
  2);
  print &ui_table_row("", "Enforce Tiki preferences hiding sensitive information", 2);
  print &ui_table_row("", "", 2);


  print &ui_table_row(
    "Unlock user",
    &ui_form_start('./tiki-user-reset.cgi', 'post', undef, ("data=unbuffered-header-processor"))
    . &ui_hidden('dom', $d->{'id'})
    . &ui_textbox("user", "", undef, undef, undef, ("placeholder='Username'"))
    . &ui_submit("Unlock", "unlock", 0)
    . &ui_form_end(),
  2);
  print &ui_table_row("", "Unlock an user blocked after several login attempts", 2);
  print &ui_table_row("", "", 2);

  print &ui_table_row(
    "Reset password",
    &ui_form_start('./tiki-user-reset.cgi', 'post', undef, ("data=unbuffered-header-processor"))
    . &ui_hidden('dom', $d->{'id'})
    . &ui_textbox("user", "", undef, undef, undef, ("placeholder='Username'"))
    . &ui_textbox("password", &tikimanager_generate_password(), "", undef, undef, ("placeholder='Password'"))
    . &ui_submit("Reset", "reset", 0)
    . &ui_form_end(),
  2);
  print &ui_table_row("", "Set a new password to the specified user", 2);
  print &ui_table_end();


  print &ui_subheading("Instance update");
  print &ui_table_start();
  print &ui_table_row("", "", 2);
  print &ui_table_row(
    "Update Tiki",
    &ui_form_start('./tiki-update.cgi', 'post', undef, ("data=unbuffered-header-processor"))
    . &ui_hidden('dom', $d->{'id'})
    . ui_submit("Update", "update", 0)
    . &ui_form_end(),
  2);
  print &ui_table_row("", "The latest changes of the branch $info->{'branch'} will be applied", 2);

  my @upgradable_to = &tikimanager_tiki_list_possible_upgrades($d, \@tiki_branches);
  if ( @upgradable_to ) {
    print &ui_table_row("", "", 2);
    print &ui_table_row(
      "Upgrade Tiki",
      &ui_form_start('./tiki-upgrade.cgi', 'post', undef, ("data=unbuffered-header-processor"))
      . &ui_hidden('dom', $d->{'id'})
      . &ui_select("branch", $latest_branch, [@upgradable_to])
      . &ui_submit("Upgrade", "upgrade", 0)
      . &ui_form_end(),
    2);
    print &ui_table_row("", "The select version of Tiki will be installed in place of the old one.", 2);
  }
  print &ui_table_row("", "", 2);
  print &ui_table_end();
}
else {
  print &ui_subheading("Import existing instance");
  print &ui_form_start('./tiki-import.cgi', 'post', undef, ("data=unbuffered-header-processor"));
  print &ui_hidden('dom', $d->{'id'});
  print &ui_table_start();
  print &ui_table_row("", "", 2);
  print &ui_table_row("Tiki found at:", "'$d->{'public_html_path'}'", 2);
  print &ui_table_row("", "", 2);
  print &ui_table_row("", "Do you want to import it?  " . ui_submit("Import", "import", 0), 2);
  print &ui_table_row("", "", 2);
  print &ui_table_end();
  print &ui_form_end();

  #
  # Install form
  #
  print &ui_subheading("Install a new Tiki instance");
  print &ui_form_start('./tiki-install.cgi', 'post', undef, ("data=unbuffered-header-processor"));
  print &ui_hidden('dom', $d->{'id'});
  print &ui_table_start();
  print &ui_table_row("", "", 2);
  print &ui_table_row("Admin email", ui_textbox('admin_email', $d->{'emailto'}, 48), 2);
  print &ui_table_row("", "Email used by the administrator to receive Tiki alerts and reset password.", 2);
  print &ui_table_row("", "", 2);
  print &ui_table_row("Sender email", ui_textbox('sender_email', $d->{'emailto'}, 48), 2);
  print &ui_table_row("", "Email to be displayed as 'sender' or 'from' for recipient users receiving emails from this Tiki", 2);
  print &ui_table_row("", "", 2);
  print &ui_table_row("Admin password", ui_textbox('password', &tikimanager_generate_password()), 2);
  print &ui_table_row("", "Copy or change the password above. You will not see that again.", 2);
  print &ui_table_row("", "", 2);
  print &ui_table_row("Select branch", &ui_select("branch", $latest_branch, [@tiki_branches]) . ui_submit("Install", "save", 0), 2);
  print &ui_table_row("Warning", &text("index_install_tiki_warning", "$d->{'public_html_path'}"), 2);
  print &ui_table_end();
  print &ui_form_end();
}

if (@cloneables) {
  #
  # Clone
  #
  print &ui_subheading("Clone from another Tiki instance");
  print &ui_form_start('./tiki-clone.cgi', 'post', undef, ("data=unbuffered-header-processor"));
  print &ui_hidden('dom', $d->{'id'});
  print &ui_table_start();
  print &ui_table_row("", "", 2);
  print &ui_table_row("", &text("index_clone_tiki_message"), 2);
  print &ui_table_row("", "", 2);
  print &ui_table_row("Select source", &ui_select("source", undef, [@cloneables]) . ui_submit("Clone", "save", 0), 2);
  print &ui_table_row("Warning", &text("index_install_tiki_warning", "$d->{'public_html_path'}"), 2);
  print &ui_table_end();
  print &ui_form_end();

  #
  # Clone and upgrade
  #
  print &ui_subheading("Clone from another Tiki instance and then upgrade");
  print &ui_form_start('./tiki-cloneandupgrade.cgi', 'post', undef, ("data=unbuffered-header-processor"));
  print &ui_hidden('dom', $d->{'id'});
  print &ui_table_start();
  print &ui_table_row("", "", 2);
  print &ui_table_row("", &text("index_clone_tiki_message"), 2);
  print &ui_table_row("", "", 2);
  print &ui_table_row("Select source", &ui_select("source", undef, [@cloneables]), 2);
  print &ui_table_row("", "", 2);
  print &ui_table_row("Select branch",  &ui_select("branch", $latest_branch, [@tiki_branches]) . ui_submit("Clone and Upgrade", "cloneandupgrade", 0), 2);
  print &ui_table_row("Warning", &text("index_install_tiki_warning", "$d->{'public_html_path'}"), 2);
  print &ui_table_end();
  print &ui_form_end();  
}

if ($info && $info->{'instance_id'}) {
  my @jobs = &cron::list_cron_jobs();
  if (@jobs) {
    print &ui_subheading("Instance Jobs");
    print &ui_columns_start([
      'active',
      'time',
      'command',
    ]);
    my $idx = 0;
    foreach my $job (@jobs) {
      if ($job->{'user'} eq $d->{'user'}) {
          my @cols;
          push(@cols, $job->{'active'} ? $text{'yes'} : "<font color=#ff0000>$text{'no'}</font>");
          push(@cols, "<code>$job->{'mins'} $job->{'hours'} $job->{'days'} $job->{'months'} $job->{'weekdays'}</code>");
          push(@cols, &ui_link("/cron/edit_cron.cgi?idx=" . $idx, $job->{'command'}));
          print &ui_columns_row(\@cols);
      }
      $idx += 1;
    }
    print &ui_columns_end();
  }
}

print <<EOF
<script type="text/javascript">
  jQuery("form[data=unbuffered-header-processor]").on("submit", function(evt) {
    evt.preventDefault();
    unbuffered_header_processor(evt, 1);
    return false;
  });
</script>
EOF
;

&ui_print_footer(
  $d ? &virtual_server::domain_footer_link($d) : ( ),
  "/virtual-server/",
  $virtual_server::text{'index_return'}
);
